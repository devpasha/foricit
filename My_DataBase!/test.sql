-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 11 2018 г., 04:40
-- Версия сервера: 5.5.53
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cars`
--

CREATE TABLE `cars` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `znak` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cars`
--

INSERT INTO `cars` (`id`, `name`, `model`, `color`, `znak`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Админ', 'BMW', 'Красный', 'ОО1234ОО', 'yes', '2018-03-10 21:59:32', '2018-03-10 21:59:32'),
(2, 'Петров Петр Петрович', 'Honda', 'Черный', 'АА1234АВ', 'yes', '2018-03-10 22:00:46', '2018-03-10 23:22:10'),
(3, 'Петров Петр Петрович', 'Reno', 'Черный', 'АI1234АВ', 'yes', '2018-03-10 22:20:26', '2018-03-10 22:32:18'),
(5, 'Васин Василий Василиевич', 'Opel', 'Металлик', 'АХ1234АВ', 'yes', '2018-03-10 22:22:44', '2018-03-10 22:22:44'),
(6, 'Васин Василий Василиевич', 'Kaen', 'white', 'АХ7777АВ', 'yes', '2018-03-10 22:23:15', '2018-03-10 23:22:43'),
(11, 'Петров Петр Петрович', 'BMW', 'Красный', 'АА9999АВ', 'no', '2018-03-10 23:24:09', '2018-03-10 23:24:15'),
(12, 'Олег Олеговчи', 'Opel', 'Синий', 'АА1234АS', 'yes', '2018-03-10 23:26:12', '2018-03-10 23:26:12');

-- --------------------------------------------------------

--
-- Структура таблицы `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `colors`
--

INSERT INTO `colors` (`id`, `color`) VALUES
(1, 'Красный'),
(2, 'Черный'),
(3, 'Металлик'),
(4, 'Синий'),
(7, 'Зеленый'),
(8, 'Желтый'),
(9, 'white');

-- --------------------------------------------------------

--
-- Структура таблицы `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `items`
--

INSERT INTO `items` (`id`, `model`) VALUES
(1, 'BMW'),
(2, 'Mercedes'),
(3, 'Opel'),
(4, 'Honda'),
(7, 'Reno'),
(8, 'Ваз'),
(9, 'Kaen');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_08_213921_create_cars_table', 1),
(4, '2018_03_09_212149_create_notes_table', 1),
(5, '2018_03_10_082501_create_colors_table', 1),
(6, '2018_03_10_082536_create_items_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `notes`
--

CREATE TABLE `notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `znak` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `notes`
--

INSERT INTO `notes` (`id`, `znak`, `comment`, `created_at`, `updated_at`) VALUES
(1, 'ОО1234ОО', 'я - админ', '2018-03-10 21:59:32', '2018-03-10 21:59:32'),
(2, 'АА1234АВ', 'первая машина', '2018-03-10 22:00:46', '2018-03-10 22:00:46'),
(3, 'АI1234АВ', 'вторая машина', '2018-03-10 22:20:26', '2018-03-10 22:20:26'),
(4, 'АА5678АС', 'снова немец!!', '2018-03-10 22:20:56', '2018-03-10 22:20:56'),
(5, 'АХ1234АВ', 'Харьков', '2018-03-10 22:22:44', '2018-03-10 22:22:44'),
(6, 'АХ7777АВ', 'красивый номер!', '2018-03-10 22:23:15', '2018-03-10 22:23:15'),
(7, 'АХ7777АВ', 'хорошо едет!', '2018-03-10 22:24:06', '2018-03-10 22:24:06'),
(8, 'АА1234АВ', 'первый заезд', '2018-03-10 22:51:55', '2018-03-10 22:51:55'),
(9, 'АА1999АВ', 'проверка', '2018-03-10 22:52:39', '2018-03-10 22:52:39'),
(10, 'АА1555АВ', '123', '2018-03-10 22:55:34', '2018-03-10 22:55:34'),
(11, 'АА1234АВ', 'тест', '2018-03-10 23:13:18', '2018-03-10 23:13:18'),
(12, 'АА1288АВ', NULL, '2018-03-10 23:13:41', '2018-03-10 23:13:41'),
(13, 'АА1000АВ', '777', '2018-03-10 23:16:40', '2018-03-10 23:16:40'),
(14, 'АА1234АВ', '1234', '2018-03-10 23:23:54', '2018-03-10 23:23:54'),
(15, 'АА9999АВ', NULL, '2018-03-10 23:24:09', '2018-03-10 23:24:09'),
(16, 'АА1234АS', '99999', '2018-03-10 23:26:12', '2018-03-10 23:26:12'),
(17, 'АА1234АS', 'huhjhjhk', '2018-03-10 23:26:55', '2018-03-10 23:26:55');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAdmin` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `isAdmin`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Админ', '1111111111', 'admin@admin.com', 'yes', '$2y$10$XR6WqrmSgvtbGw0JwWBneu4FeEwehBoDEQZfIxQIv/QyfDqu5f2SW', NULL, '2018-03-10 21:59:32', '2018-03-10 21:59:32'),
(2, 'Петров Петр Петрович', '0931122333', 'test@test.com', 'no', '$2y$10$tXWSLtc4mXHXecIInOe8NukF5ZJfLVyXFUJaKQYcdN3sqxlV/YDqa', NULL, '2018-03-10 22:00:46', '2018-03-10 22:00:46'),
(3, 'Васин Василий Василиевич', '0631234567', 'tes1@test.com', 'no', '$2y$10$GnZy6tq9IMWQ6dr4.CJsFepOOFXiINXbv/idNCD0ICJdCKDvAY1xG', NULL, '2018-03-10 22:22:44', '2018-03-10 22:22:44'),
(6, 'Олег Олеговчи', '0931992333', 't99t@test.com', 'no', '$2y$10$vLrQdoUaf.BMXDrzMmkqierspjISxjvU8S8hxJwgmDw88gg93FJMm', NULL, '2018-03-10 23:26:12', '2018-03-10 23:26:12');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cars_znak_unique` (`znak`);

--
-- Индексы таблицы `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
