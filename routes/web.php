<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('MainPage');

Route::group(['middleware'=>'guest'], function() {

    Route::get('/enter', 'Auth\RegisterController@showEnterForm')->name('EnterPage');
    Route::get('/registration', 'Auth\RegisterController@showRegistrationForm')->name('RegFormPage');
    Route::post('/registration', 'Auth\RegisterController@register')->name('RegistrationPage');

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('LogFormPage');

    Route::post('/login', 'Auth\LoginController@login')->name('LoginPage');


});
Route::group(['middleware'=>'auth'], function() {
  Route::get('/user/{name}', 'UserController@showClientPage')->name('UserList');
  Route::put('/user/{znak}', 'UserController@change');//->where(['znak'=>'[A-Z0-9A-Z]+']);
  Route::post('/addcomment/{znak}', 'UserController@addComment');
  Route::post('/addcar', 'UserController@addCar');

 Route::get('/admin', 'AdminController@showAdminList')->name('adminList');
 Route::put('/changecar/{znak}', 'AdminController@changeCar');
 Route::put('/changecolor/{znak}', 'AdminController@changeColor');
 Route::post('/addmodel', 'AdminController@addModel');
Route::post('/addcolor', 'AdminController@addColor');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

});
