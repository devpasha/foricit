<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddCarRequest;
use App\Car;
use App\Note;
use App\Color;
use App\Item;

class UserController extends Controller
{

  public function showClientPage($name) {
      $client = Car::select('model', 'color', 'znak')->where([ ['name', '=', $name], ['status', '=', 'yes'] ])->get();
      // $znak = $client->get('znak'); // почему не сработал и вернул null ???!!!!
if( empty( $client->all() ) ){ // если коллекция пустая
  return redirect()->route('MainPage');
}
      //Перобразуем в массив, что бы получить номерной знак
      $znak = $client->toArray();
      $znak = $znak[0]['znak'];
      // dd($znak);
      // $notes = Note::select('znak', 'comment')->where('znak', '=', $znak)->get();
      $notes = Note::select('znak', 'comment')->get();
      $colors = Color::select('color')->get();
      $models = Item::select('model')->get();

      return view('auth.client')->with(['b_name'=>$name,
                                        'b_client'=>$client,
                                        'b_notes'=>$notes,
                                        'b_colors'=>$colors,
                                        'b_models'=>$models,
                                        

    ]);
    }

      public function change($znak){
        $name = Car::select('name')->where('znak', '=', $znak)->get();
        $name = $name->toArray();
        $name = $name[0]['name']; // имя нужно, что бы передать в метод showClientPage
        Car::where('znak', $znak)->update(['status'=>'no']);
        return redirect()->route('UserList', [$name]);
      }

public function addComment(Request $request, $znak){
  $name = Car::select('name')->where('znak', '=', $znak)->get();
  $name = $name->toArray();
  $name = $name[0]['name'];
  Note::insert(['znak'=>$znak, 'comment'=>$request->input('addComment'), 'created_at'=>NOW(), 'updated_at'=>NOW() ]);
  return redirect()->route('UserList', [$name]);
}

   public function addCar(AddCarRequest $request) {
$name = $request->input('name');
// dd($name);
Car::insert(['name'=>$name,
              'model'=>$request->input('model'),
              'color'=>$request->input('color'),
              'znak'=>$request->input('znak'),
              'created_at'=>NOW(),
              'updated_at'=>NOW()
            ]);
Note::insert(['znak'=>$request->input('znak'),
             'comment'=>$request->input('comment'),
              'created_at'=>NOW(), 'updated_at'=>NOW()
            ]);

return redirect()->route('UserList', [$name]);
    }
}
