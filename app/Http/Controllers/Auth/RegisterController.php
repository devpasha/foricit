<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Car;
use App\Note;
use App\Color;
use App\Item;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|min:5|max:20|unique:users',
            'password' => 'required|min:6|confirmed',
            'name' => 'required|string|max:50',
            'phone' => 'required|string|max:10',
            'color' => 'required',
            'model' => 'required',
            'znak' => 'required|string|min:8|max:8|unique:cars'
            // 'password' => 'required|string|min:6|',
        ]);
    }

public function showEnterForm(){
  // die('123');

  return view('enter');
}

public function showRegistrationForm(Request $request){
  // print_r($request->input('email'));
  // echo "<br>";
  // print_r($request->input('password'));
  // dd();
  $colors = Color::select('color')->get();
  $models = Item::select('model')->get();
  return view('registration')->with(['b_email'=>$request->input('email'),
                                      'b_password'=>$request->input('password'),
                                      'b_password_conf'=>$request->input('password_confirmation'),
                                      'b_colors'=>$colors,
                                      'b_models'=>$models
                                    ]);
}

public function register(Request $request)
{
  // dd($_POST);
    try {
        $this->validator($request->all())->validate();
    }catch (\Exception $e){
        dd($e);
    }
    $name = $request->input("name");
    $phone = $request->input("phone");
    $email = $request->input("email");
    $password = $request->input("password");
    $objUser = $this->create(['name'=>$name, 'phone'=>$phone, 'email'=>$email, 'password'=>$password]);
    $model = $request->input("model");
    $color = $request->input("color");
    $znak = $request->input("znak");
    $objCar = $this->createCar(['name'=>$name, 'model'=>$model, 'color'=>$color, 'znak'=>$znak] );
    $comment = $request->input("comment");
    $objNote = $this->createNote(['znak'=>$znak, 'comment'=>$comment] );
    return redirect()->route('LogFormPage');
}
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function createCar(array $data)
    {
        return Car::create([
            'name' => $data['name'],
            'model' => $data['model'],
            'color' => $data['color'],
            'znak' => $data['znak'],
            ]);
    }

    protected function createNote(array $data)
    {
        return Note::create([
            'znak' => $data['znak'],
            'comment' => $data['comment'],
            ]);
    }
}
