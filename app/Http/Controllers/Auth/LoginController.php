<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
      return view('login');
    }

    public function login(Request $request){
      try{
         $this->validate($request, [
             'email'=>'required|string|min:3',
             'password' => 'required|min:6'
         ]);
         if(Auth::attempt(['email' => $request->input('email'), 'password'=>$request->input('password')])){
            $admin = User::select('isAdmin')->where('email','=', $request->input('email'))->get();
            $admin=$admin->toArray();
            if($admin[0]['isAdmin'] != 'yes'){
               $user = User::select('name')->where('email','=', $request->input("email") )->get();
               $user = $user->toArray();
               $user = $user[0]['name'];
              //  return redirect()->route('userList')->with(['b_user' => $user]);
               return redirect()->route('UserList',['b_user' => $user]);
              }
              else {
                  return redirect()->route('adminList');
              }
         }
         else{
             dd("Вы ввели неправильный логин или пароль");
         }

      }catch (ValidationException $e){
          dd($e);
      }
    }

    public function logout(Request $request)
    {
        $request->session()->invalidate();
        return redirect()->route('MainPage');
    }
}
