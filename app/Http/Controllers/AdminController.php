<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Car;
use App\User;
use App\Color;
use App\Item;

class AdminController extends Controller
{
    public function showAdminList(){
$colors = Color::select('color')->get();
$models = Item::select('model')->get();

$all = Car::join('users', function ($join) {
            $join->on('users.name', '=', 'cars.name')
                 ->where('users.isAdmin', '=', 'no');
        })
        ->get();
      return view('auth.admin')->with(['b_all'=>$all,
                                      'b_colors'=>$colors,
                                      'b_models'=>$models
                                    ]);
    }

    public function changeCar(Request $request, $znak){
      Car::where('znak', $znak)->update(['model'=>$request->input('newModel')]);
      return redirect()->route('adminList');
    }

    public function changeColor(Request $request, $znak){
      Car::where('znak', $znak)->update(['color'=>$request->input('newColor')]);
      return redirect()->route('adminList');
    }

    public function addModel(Request $request){
      Item::insert(['model'=>$request->input('addModel')]);
      return redirect()->route('adminList');
    }

    public function addColor(Request $request){
      Color::insert(['color'=>$request->input('addColor')]);
      return redirect()->route('adminList');
    }
}
