@extends('template.site')
@section('content')
<h3>Пожалуйста, введите Ваши данные для продолжения регистрации</h3>
<form action="{{route('RegistrationPage')}}" method="post">
  {{csrf_field()}}
  <input type="hidden" name="email" value="{{$b_email}}">
  <input type="hidden" name="password" value="{{$b_password}}">
  <input type="hidden" name="password_confirmation" value="{{$b_password_conf}}">
  <input type="text" placeholder="Ф И О" name="name"><br>
  <input type="text" placeholder="Телефон" name="phone"><br>
  <input type="text" placeholder="Номерной знак" name="znak"><br>
  <select name="model">
    @foreach($b_models as $model)
   <option value="{{$model->model}}">{{$model->model}}</option>
   @endforeach
   </select>
   <br>
   <select name="color">
     @foreach($b_colors as $color)
    <option value="{{$color->color}}">{{$color->color}}</option>
    @endforeach
    </select>
    <br>
    <input type="text" placeholder="Заметка" name="comment">
    <br>
  <input type="submit" value="Зарегестрировать" >
</form>
@endsection
