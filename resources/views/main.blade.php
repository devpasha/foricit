@extends('template.site')
@section('content')
<h1>Приветствуем Вас на главной странице нашего сайта!</h1>
<h3>Для продолжения выберите Регистрация или Войти в верхнем меню.</h3>
<div>
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
   Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
   Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
</div>
<div class="my-3 p-3 bg-white rounded box-shadow">
  <h6 class="border-bottom border-gray pb-2 mb-0">Recent updates</h6>
  <div class="media text-muted pt-3">
    <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded">
    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
      <strong class="d-block text-gray-dark">@username</strong>
      Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
    </p>
  </div>
  <small class="d-block text-right mt-3">
    <a href="#">All updates</a>
  </small>
</div>

<div class="my-3 p-3 bg-white rounded box-shadow">
  <h6 class="border-bottom border-gray pb-2 mb-0">Suggestions</h6>
  <div class="media text-muted pt-3">
    <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded">
    <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
      <div class="d-flex justify-content-between align-items-center w-100">
        <strong class="text-gray-dark">Full Name</strong>
        <a href="#">Follow</a>
      </div>
      <span class="d-block">@username</span>
    </div>
  </div>
</div>
</main>
@endsection
