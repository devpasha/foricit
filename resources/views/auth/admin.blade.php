@extends('template.site')
@section('content')
Добрый день, Админ!<br>
<table class="brd">
<tr>
<th> № </th>
<th> ФИО </th>
<th> Марка </th>
<th> Изменить марку</th>
<th> Цвет </th>
<th> Изменить цвет</th>
<th> Знак </th>
</tr>
@foreach($b_all as $all)
<tr>
  <td>
  {{$loop->index + 1}}
  </td>
  <td>
  {{$all->name}}
  </td>
  <td>
  {{$all->model}}
  </td>
  <td>
    <form action="/changecar/{{$all->znak}}" method="post">
      {{csrf_field()}}
    <select name="newModel">
      @foreach($b_models as $model)
     <option value="{{$model->model}}">{{$model->model}}</option>
     @endforeach
     </select>
     <input type="hidden" name="_method" value="PUT">
     <input type="submit" value="Изменить" >
     </form>
   </td>
<td>
{{$all->color}}
</td>
<td>
  <form action="/changecolor/{{$all->znak}}" method="post">
    {{csrf_field()}}
  <select name="newColor">
    @foreach($b_colors as $color)
   <option value="{{$color->color}}">{{$color->color}}</option>
   @endforeach
   </select>
   <input type="hidden" name="_method" value="PUT">
   <input type="submit" value="Изменить" >
   </form>
</td>
<td>
{{$all->znak}}
</td>
</tr>
@endforeach
</table>
<br>
<div>
  Добавить модель автомобиля
<form action="/addmodel" method="post">
  {{csrf_field()}}
  <input type="text" placeholder="Введите марку" name="addModel">
 <input type="submit" value="Добавить" >
 </form>
 </div>
 <div>
<div>
  Добавить цвет автомобиля
  <form action="/addcolor" method="post">
    {{csrf_field()}}
    <input type="text" placeholder="Введите цвет" name="addColor">
   <input type="submit" value="Добавить" >
   </form>
</div>
@endsection
