@extends('template.site')
@section('content')
<h2>Добрый день, {{ $b_name }}!</h2>
<h3>Ваши зарегестрированные автомобили:</h3>
<table class="brd">
<tr>
<!-- <th> ФИО </th> -->
<th> № </th>
<th> Марка </th>
<th> Цвет </th>
<th> Знак </th>
<th> Редактирование </th>
<th> Добавить комментарий </th>
</tr>
@foreach($b_client as $client)
<tr>
  <td>
  {{$loop->index + 1}}
  </td>
<td>
{{$client->model}}
</td>
<td>
{{$client->color}}
</td>
<td>
{{$client->znak}}
</td>
<td>
    <form class="form-inline my-2 my-lg-0" method="post" action="/user/{{$client->znak}}">
    {{csrf_field()}}
    {{--Для изменения данных используем метод PUT--}}
    <input type="hidden" name="_method" value="PUT">
    <input type="submit" value="Скрыть автомобиль">
    </form>
</td>
<td>
    <form method="post" action="/addcomment/{{$client->znak}}">
    {{csrf_field()}}
    <input type="text" name="addComment">
    <input type="submit" value="Добавить">
    </form>
</td>
</tr>
    @foreach($b_notes as $note)
      @if($note->znak == $client->znak )<br>
      <td><b>Коментарий:</b> <br>
          {{$note->comment}}
      </td>
      @endif
    @endforeach
@endforeach
</table>
<br>
<h3>Дoбавить новый автомобиль:</h3>

<form action="/addcar" method="post">
  {{csrf_field()}}
<input type="text" name="name" value="{{$b_name}}"><br>
<input type="text" placeholder="Номерной знак" name="znak"><br>
<select name="model">
  @foreach($b_models as $model)
 <option value="{{$model->model}}">{{$model->model}}</option>
 @endforeach
 </select>
 <br>
 <select name="color">
   @foreach($b_colors as $color)
  <option value="{{$color->color}}">{{$color->color}}</option>
  @endforeach
  </select>
  <br>
  <input type="text" placeholder="Заметка" name="comment">
  <br>
<input type="submit" value="Зарегестрировать" >
</form>

@endsection
